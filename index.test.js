// Modules
const Sharp = require('sharp');

function compressImgByContentType(type, img) {
  switch (type) {
    case 'image/jpeg': {
      return Sharp(img)
        .jpeg({ quality: 50 }) // Q 1 ~ Q 100
        .toBuffer();
    }
    case 'image/png': {
      // Because PNG is lossless filetype, it can’t be compressed more.
      // So it must be converted to JPG.
      // The original image may lost transparent effect.
      return Sharp(img)
        .jpeg({ quality: 50 })
        .toBuffer();
    }
    case 'image/gif': { // Convert GIF into JPG due to Sharp doesn't support GIF output.
      return Sharp(img)
        .jpeg({ quality: 50 })
        .toBuffer();
  }
    default: {
      return null;
    }
  }
}

function compressImgAndConvertToJPG(type, img) {
  switch (type) {
    case 'image/jpeg':
    case 'image/png':
    case 'image/gif':
      return Sharp(img)
        .jpeg({ quality: 50 }) // Q 1 ~ Q 100
        // .toBuffer();
    default: {
      return null;
    }
  }
}

function decodeKey(originalKey) {
  // Space character being converted to `+` if the image file name has a space character, e.g `my image.jpg`.
  return decodeURIComponent(originalKey).replace(/\+/g, ' ');
}

function renameKeyIntoJPG(newKey) {
  // abc.jpeg => abc.jpg ; abc.JPEG => abc.jpg
  // abc.gif => abc.jpg ; abc.GIF => abc.jpg
  // abc.png => abc.jpg ; abc.PNG => abc.jpg
  return newKey.replace(/\.(gif|jpeg|png)/i, '.jpg');
}

// compressImgByContentType('image/png', './sample/sample3.png').toFile('./after.png')

// console.log(decodeKey('my+image.jpg'));

// console.log(renameKeyIntoJPG('abc.GIF'));

compressImgAndConvertToJPG('image/jpeg', './sample/sample 4.png').toFile('./sample 4.jpg')
