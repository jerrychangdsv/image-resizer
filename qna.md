The issues:

1. Code Updating Problem:
This program need a third-party package to compress the image.
I have to zip and upload whole project within node_modules folder to AWS.
It's kinda annoying to zip and upload this project after I modify the code each time.
Because the AWS online editor only support when the project size less then 10MB.
Is there any method to solve this problem?

2. FileTypes Problem:
This program now can only handle JPEG images
If user upload and PNG image, this program can still compress the image.
But the file format will become JPEG file and the filename won't change.
So after transfer, the file now become JPEG file within a PNG filetype ex: abc.png(actually this file is now a JPEG file)

3. FileKey Problem:
I have to set a new key for S3 API, If I wanna save the compressed image to another folder or path.

For example, If the input image key is `img/sample.jpg`.
I have to save this key and then modify it into: `resized-img/sample.jpg`
It means I have to use text replacer such as Regular Expression.
To indentify the original key `img/"sample.jpg"`, and modify it into `resized-img/"sample.jpg"`
Is there and better solutation?
