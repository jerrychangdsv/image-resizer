// Modules
const AWS = require('aws-sdk');
const Sharp = require('sharp');
// S3
const S3 = new AWS.S3();

function decodeKey(originalKey) {
  // Space character being converted to `+` if the image file name has a space character, e.g `my image.jpg`.
  return decodeURIComponent(originalKey).replace(/\+/g, ' ');
}

function compressImgAndConvertToJPG(type, img) {
  switch (type) {
    case 'image/jpeg':
    case 'image/png':
    case 'image/gif':
      return Sharp(img)
        .jpeg({ quality: 50 }) // Q 1 ~ Q 100
        .toBuffer();
    default: {
      return null;
    }
  }
}

exports.handler = async function (event, context, callback) {
  console.log('Executing Image Resizer');

  const [{ s3 }] = event.Records;
  const Bucket = s3.bucket.name;
  const Key = decodeKey(s3.object.key);

  console.log(`Bucket: ${Bucket}`);
  console.log(`Key: ${Key}`);

  const { ContentType, Body } = await S3
    .getObject({
      Bucket,
      Key
    })
    .promise()
    .catch((err) => context.fail(`[S3] getObject fail: ${err}`));

  console.log(`ContentType: ${ContentType}`);

  const newBody = await compressImgAndConvertToJPG(ContentType, Body)
    .catch((err) => context.fail(`Compress fail: ${err}`));

  if (newBody) {
    // resized-img/img/sample.jpg => resized-img/sample.jpg
    const newKey = `resized-img/${Key.replace('img/', '')}`;
    // abc.jpeg => abc.jpg ; abc.JPEG => abc.jpg
    // abc.gif => abc.jpg ; abc.GIF => abc.jpg
    // abc.png => abc.jpg ; abc.PNG => abc.jpg
    const renamedKey = newKey.replace(/\.(gif|jpeg|png)/i, '.jpg');

    console.log(`New Key: ${newKey}`);

    const result = await S3
      .putObject({
        Body: newBody,
        Bucket,
        ContentType: 'image/jpeg',
        Key: renamedKey
      })
      .promise()
      .catch((err) => context.fail(`[S3] putObject fail: ${err}`));

    context.succeed('Image compress succeed');
  } else {
    context.fail('Invalid content type');
  }
};
